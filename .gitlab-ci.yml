# Je n'arrive pas à passer l'erreur suivante lors du lancement du pipeline:
# fatal: unable to access 'https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@192.168.103.4/taskforce-nncr/ansible.git/': Peer's certificate issuer has been marked as not trusted by the user.
# Doc: https://docs.gitlab.com/runner/configuration/tls-self-signed.html
# A defaut de solution, pour l'instant, je désactive la vérif du certificat:
variables:
  GIT_SSL_NO_VERIFY: "1"
  GIT_SUBMODULE_STRATEGY: recursive

stages:
  - Check
  - Simulation
  - DB
  - Galaxy
  - Stage3
  - Stage4

# Global before jobs
# Use to list all playbooks for check stage (removal of "playbook_site.yml")
# Extracted from this file for each environment and played in the same order
before_script:
  - export LIST_PLAYBOOKS_DEV=$(grep ' development' .gitlab-ci.yml | grep -v -E '( -C|-check| -t|--tags)' | grep -o -E 'playbook_.*.yml')
  - export LIST_PLAYBOOKS_PREPROD=$(grep ' preproduction' .gitlab-ci.yml | grep -v -E '( -C|-check| -t|--tags)' | grep -o -E 'playbook_.*.yml')
  - export LIST_PLAYBOOKS_PROD=$(grep ' production'  .gitlab-ci.yml | grep -v -E '( -C|-check| -t|--tags)' | grep -o -E 'playbook_.*.yml')
  - ansible-galaxy install --force -r requirements.yml


########## Templates ##########

.feature:
  except:
    - preprod
    - master
  # Uncomment "only" only during release preparation phasis to skip the CI if not linked to the release
  #   If uncomment, the branch or the commit message must content the world "release" to run the CI
  only:
    variables:
      - $CI_COMMIT_REF_NAME =~ /.*release.*/i
      - $CI_COMMIT_MESSAGE =~ /.*release.*/i
  tags:
    - igbmc
    - shell

.preprod:
  only:
    - preprod
  tags:
    - igbmc
    - shell

.prod:
  only:
    - master
  tags:
    - idris
    - shell


########## Check ##########

Syntax (feature):
  extends: .feature
  stage: Check
  script: "ansible-playbook -v -i development --syntax-check $LIST_PLAYBOOKS_DEV"

Syntax (preprod):
  extends: .preprod
  stage: Check
  script: "ansible-playbook -i preproduction --vault-id ~/.ansible_vault-password_ifb-nncr --syntax-check $LIST_PLAYBOOKS_PREPROD"

Syntax (prod):
  extends: .prod
  stage: Check
  script: "ansible-playbook -i production --vault-id ~/.ansible_vault-password_ifb-nncr --syntax-check $LIST_PLAYBOOKS_PROD"


########## Simulation ##########

Check (feature):
  extends: .feature
  stage: Simulation
  script: "ansible-playbook -i development --check --diff $LIST_PLAYBOOKS_DEV"
  when: manual

Check (preprod):
  stage: Simulation
  script: "ansible-playbook -i preproduction --vault-id ~/.ansible_vault-password_ifb-nncr --vault-id ~/.ansible_vault-password_ifb-nncr --check --diff $LIST_PLAYBOOKS_PREPROD"
  when: manual
  except:
    - master@taskforce-nncr/ansible
  tags:
    - igbmc

Check (prod):
  stage: Simulation
  script: "ansible-playbook -i production --vault-id ~/.ansible_vault-password_ifb-nncr --vault-id ~/.ansible_vault-password_ifb-nncr --check --diff $LIST_PLAYBOOKS_PROD"
  when: manual
  except:
    - master@taskforce-nncr/ansible
  tags:
    - idris


########## DB ##########

Postgres for Galaxy (feature):
  extends: .feature
  stage: DB
  script: "ansible-playbook -i development playbook_galaxy-db.yml --diff"

Postgres for Galaxy (preprod):
  extends: .preprod
  stage: DB
  script: "ansible-playbook -i preproduction --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy-db.yml --diff"

Postgres for Galaxy (prod):
  extends: .prod
  stage: DB
  script: "ansible-playbook -i production --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy-db.yml --diff"

######### Galaxy ##########

Galaxy (feature):
  extends: .feature
  stage: Galaxy
  script: "ansible-playbook -i development playbook_galaxy.yml"

Galaxy (preprod):
  extends: .preprod
  stage: Galaxy
  script: "ansible-playbook -i preproduction --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy.yml"

Galaxy (prod):
  extends: .prod
  stage: Galaxy
  script: "ansible-playbook -i production --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy.yml"


CVMFS on HPC nodes (feature):
  extends: .feature
  stage: Galaxy
  script: "ansible-playbook -i development playbook_hpc_cvmfs.yml --diff"

CVMFS on HPC nodes (preprod):
  extends: .preprod
  stage: Galaxy
  script: "ansible-playbook -i preproduction --vault-id ~/.ansible_vault-password_ifb-nncr playbook_hpc_cvmfs.yml --diff"

CVMFS on HPC nodes (prod):
  extends: .prod
  stage: Galaxy
  script: "ansible-playbook -i production --vault-id ~/.ansible_vault-password_ifb-nncr playbook_hpc_cvmfs.yml --diff"

######### Stage3 ##############

Nginx for Galaxy (feature):
  extends: .feature
  stage: Stage3
  script: "ansible-playbook -i development playbook_galaxy-nginx.yml --diff"

Nginx for Galaxy (preprod):
  extends: .preprod
  stage: Stage3
  script: "ansible-playbook -i preproduction --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy-nginx.yml --diff"

Nginx for Galaxy (prod):
  extends: .prod
  stage: Stage3
  script: "ansible-playbook -i production --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy-nginx.yml --diff"

Extra for Galaxy (feature):
  extends: .feature
  stage: Stage3
  script: "ansible-playbook -i development playbook_galaxy-extra.yml --diff"

Extra for Galaxy (preprod):
  extends: .preprod
  stage: Stage3
  script: "ansible-playbook -i preproduction --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy-extra.yml --diff"

Extra for Galaxy (prod):
  extends: .prod
  stage: Stage3
  script: "ansible-playbook -i production --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy-extra.yml --diff"

######### Stage4  ##############

Proftp for Galaxy (feature):
  extends: .feature
  stage: Stage4
  script: "ansible-playbook -i development playbook_galaxy-proftp.yml --diff"

Proftp for Galaxy (preprod):
  extends: .preprod
  stage: Stage4
  script: "ansible-playbook -i preproduction --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy-proftp.yml --diff"

Proftp for Galaxy (prod):
  extends: .prod
  stage: Stage4
  script: "ansible-playbook -i production --vault-id ~/.ansible_vault-password_ifb-nncr playbook_galaxy-proftp.yml --diff"
